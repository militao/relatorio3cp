<?php

require_once  'index.php';

use PHPUnit\Framework\TestCase;

class IndexSuc extends TestCase
{
    public function testHello()
    {
        $obj = new Hello();
        $resultado = $obj->hello("Hello World!");

        $this->assertEquals("Hello World!", $resultado);
    }
}
