<?php

require_once  'index.php';

use PHPUnit\Framework\TestCase;

class IndexFalha extends TestCase
{
    public function testHello()
    {
        $obj = new Hello();
        $resultado = $obj->hello("Hello World!");

        $this->assertEquals("Falha", $resultado);
    }
}
