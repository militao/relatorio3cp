# LINK
Utilizamos o domínio nip.io para testar a URL:
http://apache.35.225.238.251.nip.io/

# Estrutura de pasta
- /manifests: Contém os manifestos do Kubernetes
- /tests: Contém os testes
- Dockerfile: Utiliza uma imagem configurada com o Apache para executar PHP
- composer.json: Dependências do PHP

# Sobre a aplicação
A aplicação (index.php) possui apenas uma função, que imprime "Hello World!".


## Index.php
<pre>
<\?php
class Hello{
  public static function hello($e){
    return $e;
  }
}
echo Hello::hello("Hello World!");
</pre>

## Usamos essa imagem
<pre>
FROM php:apache
COPY . /var/www/html/
CMD ["apache2-foreground"]
</pre>

# RESUMO
Ao realizar o commit na branch principal (main), aciona-se a pipeline, composta por três jobs:
 - Build: Buildará a imagem e salvará no Artifact Registry no GCP para versionamento
 - Test: Utiliza o phpunit para testar a aplicação
 - Deploy: Executa os manifestos para Ingress, Deployment (que obtém a imagem do Registry) e Service no cluster do GKE.
Usaremos Helm para o nginx-ingress que gerenciará a rede e o dns(apache.35.225.238.251.nip.io/)

# Manifestos
- manifest-deployment.yml
  - Cria um Deployment de apenas uma replica, expondo a porta 80 do container
- manifest-ingress.yml
  - Define as regras e aplica somente para o host "apache.__MY_IP__.nip.io"
manifest-service.yml
  - Expõe a porta 80 do meu Service e direciona o trafego para a porta 80 do Deployment

# A pipeline
<pre>
variables:
  IMAGE_NAME: "teste3cpimage" # O nome da imagem
  TAG: $CI_COMMIT_SHA # Usaremos o commit como tag


job_build:
  stage: build
  only:
    - main
  image: docker:latest # Como iremos manipular uma imagem docker, usamos o docker in docker(dind)
  services:
    - docker:dind
  before_script: # Autentica no Artifact Registry
    - echo $GCP_CREDENTIALS | docker login -u _json_key --password-stdin https://$GCP_REGISTRY_HOST
  
  script: # Builda a imagem, adiciona a tag e push para o Artifact Registry, que versiona as builds
    - docker build -t $IMAGE_NAME .
    - docker tag "$IMAGE_NAME" "$GCP_REGISTRY_HOST/$GCP_PROJECT_ID/$GCP_REGISTRY_NAME/$IMAGE_NAME:$TAG"
    - docker push "$GCP_REGISTRY_HOST/$GCP_PROJECT_ID/$GCP_REGISTRY_NAME/$IMAGE_NAME:$TAG"
</pre>

<pre>
job_test:
  stage: test
  only:
    - main
  image: docker:latest # Como iremos manipular uma imagem docker usamos o docker in docker(dind)
  services:
    - docker:dind
  
  # Autentica no Artifact Registry e obtem a imagem no registry que foi upada no job anterior e cria um container
  #(Poderiamos também utilizar artifacts para compartilhar as imagens entre os jobs)
  before_script: 
    - echo $GCP_CREDENTIALS | docker login -u _json_key --password-stdin https://$GCP_REGISTRY_HOST
    - docker run -d --name app "$GCP_REGISTRY_HOST/$GCP_PROJECT_ID/$GCP_REGISTRY_NAME/$IMAGE_NAME:$TAG"

  # Instalamos as dependencias, composer e dependencias doc composer.json(phpunit para os testes).
  # Comandos separados para melhor visualização
  script: 
    - docker exec app bash -c "cd /var/www/html"
    - docker exec app bash -c "apt update -y"
    - docker exec app bash -c "apt install curl unzip -y"
    - docker exec app bash -c "curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer"
    - docker exec app bash -c "composer install" 

    # O phpunit executa os teste, temos 2 testes, um para sucesso e outro falha(que está comentado)
    #- docker exec app bash -c "vendor/bin/phpunit IndexFalha.spec.php"
    - docker exec app bash -c "vendor/bin/phpunit tests/IndexSuc.spec.php"
</pre>


<pre>
#Se passou pelos jobs anteriores e está na branch main sabemos que esta pronto para a produção, então podemos uparmos no kubernetes.
job_deploy:
  stage: deploy
  only:
    - main
  image: google/cloud-sdk # Possui a maioria das dependencias gcloud

  before_script: #Autenticamos no cluster Kubernetes
    - echo $GCP_CREDENTIALS > /tmp/$CI_PIPELINE_ID.json
    - gcloud auth activate-service-account --key-file /tmp/$CI_PIPELINE_ID.json
    - gcloud config set project $GCP_PROJECT_ID
    - gcloud container clusters get-credentials teste3cp-cluster --region=us-central1
    
    - apt install jq -y \# Instalamos para manipularmos o yaml


  script:
    - git clone $CI_REPOSITORY_URL kube_repo   #Clonamos o repositorio atual
    - cd kube_repo/manifest   # Pegamos os manifestos
    - sed -i "s/__MY_IMAGE__/$GCP_REGISTRY_HOST\/$GCP_PROJECT_ID\/$GCP_REGISTRY_NAME\/$IMAGE_NAME:$TAG/" manifest-deployment.yml  
        #Troco __MY_IMAGE__ que está no manifesto pela imagem e tag atual
    - sed -i "s/__MY_IP__/$(kubectl -n ingress-nginx get service nginx-control-nginx-ingress-controller -o json | jq -r '.status.loadBalancer.ingress[0].ip')/" manifest-ingress.yml 
        # O comando entre $() obtem o ip do meu ingress nginx para substituirmos o __MY_IP__ do manifesto ingress para gerenciar o ip correto 
    - kubectl apply -f .   #Roda os manifestos no namespace default(como tem apenas uma aplicação no cluster)
</pre>


# TESTAR
- Clone esse repositorio para seu repositorio no gitlab
- Crie um projeto no GCP
- Permita acesso no IAM, deve possuir os seguintes papeis(usado para autenticação externa):
    - Administrador do Artifact Registry
    - Administrador do Kubernetes Engine
- Baixe a chave de acesso JSON desse IAM
- Crie o cluster Kubernetes:
    - No menu Kubernetes Engine crie um cluster(pode ser Autopilot)
    - Va nas opções do cluster e pegue o código para conectar com o cluster, exemplo: 
        - gcloud container clusters get-credentials teste --region us-central1 --project teste-411019
- Crie um Artifact Registry para versionarmos as imagens da pipeline
- Na sua máquina local
    - Tenha docker instalado
    - Use a imagem do google/cloud-sdk do dockerhub, que ja contem a maioria da dependencias necessarias: 
        - docker run -it --name google google/cloud
    - Copie o JSON do IAM para dentro do container: 
        - docker cp <ARQUIVO_IAM>.json google:/home/cloudsdk
    - Obtenha o shell desse container
        - docker exec -it google /bin/bash
    - Agora dentro do container, precisamos do Helm(gerenciador de pacotes para o kubernetes), execute:
        - curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
        - chmod 700 get_helm.sh
        - ./get_helm.sh
    - Cole o comando de conexao do cluster **gcloud container clusters get-credentials...** (Agora estamos autenticados e obtemos os arquivos necessarios para manipularmos o kubernetes)
    - Agora precisamos instalar o ingress nginx no cluster, o ingress é um recurso que gerencia e permite o acesso externo aos serviços do cluster, controlando o trafego de rede, execute:
        - helm repo add nginx-stable https://helm.nginx.com/stable
        - kubectl create namespace ingress-nginx
        - helm install nginx-control nginx-stable/nginx-ingress -n ingress-nginx
    - Precisamos nos autenticar no Artifact Registry para obtermos as imagens versionadas pela pipeline para executarmos os pods, então definimos as secrets: 
        - kubectl create secret docker-registry gcr-pull-secret \
        -\-docker-server=<servidor do registry, exemplo: us-east1-docker.pkg.dev> \
        -\-docker-username=_json_key \
        -\-docker-password="$(cat <ARQUIVO_IAM>.json)" \
- Adicione as variaveis para a pipeline, em setting->ci/cd->variables: GCP_CLUSTER_NAME, GCP_CLUSTER_REGION, GCP_CREDENTIALS, GCP_PROJECT_ID, GCP_REGISTRY_HOST, GCP_REGISTRY_NAME
- Agora Basta executar a pipeline
